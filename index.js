let	person = {
	firstName: 'John',
	lastName: 'Smith',
	age: 30,
	isMarried: true,
	hobbies: ['Biking', 'Mountain Climbing', 'Swimming'],
	workAddress: {
		houseNumber: '32',
		street: 'Washington',
		city: 'Lincoln',
		state: 'Nebraska'
	}
};
console.log("First Name: " + person.firstName);
console.log("Last Name: " + person.lastName);
console.log("Age: " + person.age);
console.log("Hobbies:");
console.log(person.hobbies);
console.log("Work Address:");
console.log(person.workAddress);

function printUserInfo(firstName, lastName, age){
	console.log (firstName + ' ' + lastName + ' is ' + age + ' years of age.');
	console.log("This was printed inside of the function");
	console.log(person.hobbies);
	console.log("This was printed inside of the function");
	console.log(person.workAddress);
};
printUserInfo(person.firstName,person.lastName,person.age);

function returnFunction (value) {
	return value
}

let returnedValue = returnFunction(person.isMarried)
console.log("The value of isMarried is: " + returnedValue)
